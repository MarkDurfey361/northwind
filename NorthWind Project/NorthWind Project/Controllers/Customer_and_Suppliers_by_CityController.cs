﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Customer_and_Suppliers_by_CityController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Customer_and_Suppliers_by_City
        public async Task<ActionResult> Index()
        {
            return View(await db.Customer_and_Suppliers_by_City.ToListAsync());
        }

        // GET: Customer_and_Suppliers_by_City/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_and_Suppliers_by_City customer_and_Suppliers_by_City = await db.Customer_and_Suppliers_by_City.FindAsync(id);
            if (customer_and_Suppliers_by_City == null)
            {
                return HttpNotFound();
            }
            return View(customer_and_Suppliers_by_City);
        }

        // GET: Customer_and_Suppliers_by_City/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer_and_Suppliers_by_City/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "City,CompanyName,ContactName,Relationship")] Customer_and_Suppliers_by_City customer_and_Suppliers_by_City)
        {
            if (ModelState.IsValid)
            {
                db.Customer_and_Suppliers_by_City.Add(customer_and_Suppliers_by_City);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(customer_and_Suppliers_by_City);
        }

        // GET: Customer_and_Suppliers_by_City/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_and_Suppliers_by_City customer_and_Suppliers_by_City = await db.Customer_and_Suppliers_by_City.FindAsync(id);
            if (customer_and_Suppliers_by_City == null)
            {
                return HttpNotFound();
            }
            return View(customer_and_Suppliers_by_City);
        }

        // POST: Customer_and_Suppliers_by_City/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "City,CompanyName,ContactName,Relationship")] Customer_and_Suppliers_by_City customer_and_Suppliers_by_City)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer_and_Suppliers_by_City).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(customer_and_Suppliers_by_City);
        }

        // GET: Customer_and_Suppliers_by_City/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer_and_Suppliers_by_City customer_and_Suppliers_by_City = await db.Customer_and_Suppliers_by_City.FindAsync(id);
            if (customer_and_Suppliers_by_City == null)
            {
                return HttpNotFound();
            }
            return View(customer_and_Suppliers_by_City);
        }

        // POST: Customer_and_Suppliers_by_City/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Customer_and_Suppliers_by_City customer_and_Suppliers_by_City = await db.Customer_and_Suppliers_by_City.FindAsync(id);
            db.Customer_and_Suppliers_by_City.Remove(customer_and_Suppliers_by_City);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
