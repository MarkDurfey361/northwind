﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Products_by_CategoryController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Products_by_Category
        public async Task<ActionResult> Index()
        {
            return View(await db.Products_by_Category.ToListAsync());
        }

        // GET: Products_by_Category/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_by_Category products_by_Category = await db.Products_by_Category.FindAsync(id);
            if (products_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(products_by_Category);
        }

        // GET: Products_by_Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products_by_Category/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CategoryName,ProductName,QuantityPerUnit,UnitsInStock,Discontinued")] Products_by_Category products_by_Category)
        {
            if (ModelState.IsValid)
            {
                db.Products_by_Category.Add(products_by_Category);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(products_by_Category);
        }

        // GET: Products_by_Category/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_by_Category products_by_Category = await db.Products_by_Category.FindAsync(id);
            if (products_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(products_by_Category);
        }

        // POST: Products_by_Category/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CategoryName,ProductName,QuantityPerUnit,UnitsInStock,Discontinued")] Products_by_Category products_by_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(products_by_Category).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(products_by_Category);
        }

        // GET: Products_by_Category/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_by_Category products_by_Category = await db.Products_by_Category.FindAsync(id);
            if (products_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(products_by_Category);
        }

        // POST: Products_by_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Products_by_Category products_by_Category = await db.Products_by_Category.FindAsync(id);
            db.Products_by_Category.Remove(products_by_Category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
