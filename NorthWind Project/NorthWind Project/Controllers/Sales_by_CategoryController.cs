﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Sales_by_CategoryController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Sales_by_Category
        public async Task<ActionResult> Index()
        {
            return View(await db.Sales_by_Category.ToListAsync());
        }

        // GET: Sales_by_Category/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_by_Category sales_by_Category = await db.Sales_by_Category.FindAsync(id);
            if (sales_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(sales_by_Category);
        }

        // GET: Sales_by_Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sales_by_Category/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CategoryID,CategoryName,ProductName,ProductSales")] Sales_by_Category sales_by_Category)
        {
            if (ModelState.IsValid)
            {
                db.Sales_by_Category.Add(sales_by_Category);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sales_by_Category);
        }

        // GET: Sales_by_Category/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_by_Category sales_by_Category = await db.Sales_by_Category.FindAsync(id);
            if (sales_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(sales_by_Category);
        }

        // POST: Sales_by_Category/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CategoryID,CategoryName,ProductName,ProductSales")] Sales_by_Category sales_by_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sales_by_Category).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sales_by_Category);
        }

        // GET: Sales_by_Category/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_by_Category sales_by_Category = await db.Sales_by_Category.FindAsync(id);
            if (sales_by_Category == null)
            {
                return HttpNotFound();
            }
            return View(sales_by_Category);
        }

        // POST: Sales_by_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sales_by_Category sales_by_Category = await db.Sales_by_Category.FindAsync(id);
            db.Sales_by_Category.Remove(sales_by_Category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
