﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Order_Details_ExtendedController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Order_Details_Extended
        public async Task<ActionResult> Index()
        {
            return View(await db.Order_Details_Extended.ToListAsync());
        }

        // GET: Order_Details_Extended/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Details_Extended order_Details_Extended = await db.Order_Details_Extended.FindAsync(id);
            if (order_Details_Extended == null)
            {
                return HttpNotFound();
            }
            return View(order_Details_Extended);
        }

        // GET: Order_Details_Extended/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order_Details_Extended/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrderID,ProductID,ProductName,UnitPrice,Quantity,Discount,ExtendedPrice")] Order_Details_Extended order_Details_Extended)
        {
            if (ModelState.IsValid)
            {
                db.Order_Details_Extended.Add(order_Details_Extended);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(order_Details_Extended);
        }

        // GET: Order_Details_Extended/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Details_Extended order_Details_Extended = await db.Order_Details_Extended.FindAsync(id);
            if (order_Details_Extended == null)
            {
                return HttpNotFound();
            }
            return View(order_Details_Extended);
        }

        // POST: Order_Details_Extended/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrderID,ProductID,ProductName,UnitPrice,Quantity,Discount,ExtendedPrice")] Order_Details_Extended order_Details_Extended)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_Details_Extended).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order_Details_Extended);
        }

        // GET: Order_Details_Extended/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Details_Extended order_Details_Extended = await db.Order_Details_Extended.FindAsync(id);
            if (order_Details_Extended == null)
            {
                return HttpNotFound();
            }
            return View(order_Details_Extended);
        }

        // POST: Order_Details_Extended/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order_Details_Extended order_Details_Extended = await db.Order_Details_Extended.FindAsync(id);
            db.Order_Details_Extended.Remove(order_Details_Extended);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
