﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Products_Above_Average_PriceController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Products_Above_Average_Price
        public async Task<ActionResult> Index()
        {
            return View(await db.Products_Above_Average_Price.ToListAsync());
        }

        // GET: Products_Above_Average_Price/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_Above_Average_Price products_Above_Average_Price = await db.Products_Above_Average_Price.FindAsync(id);
            if (products_Above_Average_Price == null)
            {
                return HttpNotFound();
            }
            return View(products_Above_Average_Price);
        }

        // GET: Products_Above_Average_Price/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products_Above_Average_Price/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProductName,UnitPrice")] Products_Above_Average_Price products_Above_Average_Price)
        {
            if (ModelState.IsValid)
            {
                db.Products_Above_Average_Price.Add(products_Above_Average_Price);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(products_Above_Average_Price);
        }

        // GET: Products_Above_Average_Price/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_Above_Average_Price products_Above_Average_Price = await db.Products_Above_Average_Price.FindAsync(id);
            if (products_Above_Average_Price == null)
            {
                return HttpNotFound();
            }
            return View(products_Above_Average_Price);
        }

        // POST: Products_Above_Average_Price/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProductName,UnitPrice")] Products_Above_Average_Price products_Above_Average_Price)
        {
            if (ModelState.IsValid)
            {
                db.Entry(products_Above_Average_Price).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(products_Above_Average_Price);
        }

        // GET: Products_Above_Average_Price/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Products_Above_Average_Price products_Above_Average_Price = await db.Products_Above_Average_Price.FindAsync(id);
            if (products_Above_Average_Price == null)
            {
                return HttpNotFound();
            }
            return View(products_Above_Average_Price);
        }

        // POST: Products_Above_Average_Price/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Products_Above_Average_Price products_Above_Average_Price = await db.Products_Above_Average_Price.FindAsync(id);
            db.Products_Above_Average_Price.Remove(products_Above_Average_Price);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
