﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Product_Sales_for_1997Controller : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Product_Sales_for_1997
        public async Task<ActionResult> Index()
        {
            return View(await db.Product_Sales_for_1997.ToListAsync());
        }

        // GET: Product_Sales_for_1997/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product_Sales_for_1997 product_Sales_for_1997 = await db.Product_Sales_for_1997.FindAsync(id);
            if (product_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(product_Sales_for_1997);
        }

        // GET: Product_Sales_for_1997/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product_Sales_for_1997/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CategoryName,ProductName,ProductSales")] Product_Sales_for_1997 product_Sales_for_1997)
        {
            if (ModelState.IsValid)
            {
                db.Product_Sales_for_1997.Add(product_Sales_for_1997);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(product_Sales_for_1997);
        }

        // GET: Product_Sales_for_1997/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product_Sales_for_1997 product_Sales_for_1997 = await db.Product_Sales_for_1997.FindAsync(id);
            if (product_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(product_Sales_for_1997);
        }

        // POST: Product_Sales_for_1997/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CategoryName,ProductName,ProductSales")] Product_Sales_for_1997 product_Sales_for_1997)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product_Sales_for_1997).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(product_Sales_for_1997);
        }

        // GET: Product_Sales_for_1997/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product_Sales_for_1997 product_Sales_for_1997 = await db.Product_Sales_for_1997.FindAsync(id);
            if (product_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(product_Sales_for_1997);
        }

        // POST: Product_Sales_for_1997/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Product_Sales_for_1997 product_Sales_for_1997 = await db.Product_Sales_for_1997.FindAsync(id);
            db.Product_Sales_for_1997.Remove(product_Sales_for_1997);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
