﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Current_Product_ListController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Current_Product_List
        public async Task<ActionResult> Index()
        {
            return View(await db.Current_Product_List.ToListAsync());
        }

        // GET: Current_Product_List/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Current_Product_List current_Product_List = await db.Current_Product_List.FindAsync(id);
            if (current_Product_List == null)
            {
                return HttpNotFound();
            }
            return View(current_Product_List);
        }

        // GET: Current_Product_List/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Current_Product_List/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProductID,ProductName")] Current_Product_List current_Product_List)
        {
            if (ModelState.IsValid)
            {
                db.Current_Product_List.Add(current_Product_List);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(current_Product_List);
        }

        // GET: Current_Product_List/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Current_Product_List current_Product_List = await db.Current_Product_List.FindAsync(id);
            if (current_Product_List == null)
            {
                return HttpNotFound();
            }
            return View(current_Product_List);
        }

        // POST: Current_Product_List/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProductID,ProductName")] Current_Product_List current_Product_List)
        {
            if (ModelState.IsValid)
            {
                db.Entry(current_Product_List).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(current_Product_List);
        }

        // GET: Current_Product_List/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Current_Product_List current_Product_List = await db.Current_Product_List.FindAsync(id);
            if (current_Product_List == null)
            {
                return HttpNotFound();
            }
            return View(current_Product_List);
        }

        // POST: Current_Product_List/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Current_Product_List current_Product_List = await db.Current_Product_List.FindAsync(id);
            db.Current_Product_List.Remove(current_Product_List);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
