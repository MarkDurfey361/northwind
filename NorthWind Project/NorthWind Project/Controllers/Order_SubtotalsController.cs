﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Order_SubtotalsController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Order_Subtotals
        public async Task<ActionResult> Index()
        {
            return View(await db.Order_Subtotals.ToListAsync());
        }

        // GET: Order_Subtotals/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Subtotals order_Subtotals = await db.Order_Subtotals.FindAsync(id);
            if (order_Subtotals == null)
            {
                return HttpNotFound();
            }
            return View(order_Subtotals);
        }

        // GET: Order_Subtotals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Order_Subtotals/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrderID,Subtotal")] Order_Subtotals order_Subtotals)
        {
            if (ModelState.IsValid)
            {
                db.Order_Subtotals.Add(order_Subtotals);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(order_Subtotals);
        }

        // GET: Order_Subtotals/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Subtotals order_Subtotals = await db.Order_Subtotals.FindAsync(id);
            if (order_Subtotals == null)
            {
                return HttpNotFound();
            }
            return View(order_Subtotals);
        }

        // POST: Order_Subtotals/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrderID,Subtotal")] Order_Subtotals order_Subtotals)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_Subtotals).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order_Subtotals);
        }

        // GET: Order_Subtotals/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Subtotals order_Subtotals = await db.Order_Subtotals.FindAsync(id);
            if (order_Subtotals == null)
            {
                return HttpNotFound();
            }
            return View(order_Subtotals);
        }

        // POST: Order_Subtotals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order_Subtotals order_Subtotals = await db.Order_Subtotals.FindAsync(id);
            db.Order_Subtotals.Remove(order_Subtotals);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
