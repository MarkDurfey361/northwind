﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Category_Sales_for_1997Controller : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Category_Sales_for_1997
        public async Task<ActionResult> Index()
        {
            return View(await db.Category_Sales_for_1997.ToListAsync());
        }

        // GET: Category_Sales_for_1997/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category_Sales_for_1997 category_Sales_for_1997 = await db.Category_Sales_for_1997.FindAsync(id);
            if (category_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(category_Sales_for_1997);
        }

        // GET: Category_Sales_for_1997/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category_Sales_for_1997/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CategoryName,CategorySales")] Category_Sales_for_1997 category_Sales_for_1997)
        {
            if (ModelState.IsValid)
            {
                db.Category_Sales_for_1997.Add(category_Sales_for_1997);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(category_Sales_for_1997);
        }

        // GET: Category_Sales_for_1997/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category_Sales_for_1997 category_Sales_for_1997 = await db.Category_Sales_for_1997.FindAsync(id);
            if (category_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(category_Sales_for_1997);
        }

        // POST: Category_Sales_for_1997/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "CategoryName,CategorySales")] Category_Sales_for_1997 category_Sales_for_1997)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category_Sales_for_1997).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(category_Sales_for_1997);
        }

        // GET: Category_Sales_for_1997/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category_Sales_for_1997 category_Sales_for_1997 = await db.Category_Sales_for_1997.FindAsync(id);
            if (category_Sales_for_1997 == null)
            {
                return HttpNotFound();
            }
            return View(category_Sales_for_1997);
        }

        // POST: Category_Sales_for_1997/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Category_Sales_for_1997 category_Sales_for_1997 = await db.Category_Sales_for_1997.FindAsync(id);
            db.Category_Sales_for_1997.Remove(category_Sales_for_1997);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
