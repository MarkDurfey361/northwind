﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Orders_QryController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Orders_Qry
        public async Task<ActionResult> Index()
        {
            return View(await db.Orders_Qry.ToListAsync());
        }

        // GET: Orders_Qry/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Orders_Qry orders_Qry = await db.Orders_Qry.FindAsync(id);
            if (orders_Qry == null)
            {
                return HttpNotFound();
            }
            return View(orders_Qry);
        }

        // GET: Orders_Qry/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders_Qry/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "OrderID,CustomerID,EmployeeID,OrderDate,RequiredDate,ShippedDate,ShipVia,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry,CompanyName,Address,City,Region,PostalCode,Country")] Orders_Qry orders_Qry)
        {
            if (ModelState.IsValid)
            {
                db.Orders_Qry.Add(orders_Qry);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(orders_Qry);
        }

        // GET: Orders_Qry/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Orders_Qry orders_Qry = await db.Orders_Qry.FindAsync(id);
            if (orders_Qry == null)
            {
                return HttpNotFound();
            }
            return View(orders_Qry);
        }

        // POST: Orders_Qry/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "OrderID,CustomerID,EmployeeID,OrderDate,RequiredDate,ShippedDate,ShipVia,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry,CompanyName,Address,City,Region,PostalCode,Country")] Orders_Qry orders_Qry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orders_Qry).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(orders_Qry);
        }

        // GET: Orders_Qry/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Orders_Qry orders_Qry = await db.Orders_Qry.FindAsync(id);
            if (orders_Qry == null)
            {
                return HttpNotFound();
            }
            return View(orders_Qry);
        }

        // POST: Orders_Qry/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Orders_Qry orders_Qry = await db.Orders_Qry.FindAsync(id);
            db.Orders_Qry.Remove(orders_Qry);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
