﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Sales_Totals_by_AmountController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Sales_Totals_by_Amount
        public async Task<ActionResult> Index()
        {
            return View(await db.Sales_Totals_by_Amount.ToListAsync());
        }

        // GET: Sales_Totals_by_Amount/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_Totals_by_Amount sales_Totals_by_Amount = await db.Sales_Totals_by_Amount.FindAsync(id);
            if (sales_Totals_by_Amount == null)
            {
                return HttpNotFound();
            }
            return View(sales_Totals_by_Amount);
        }

        // GET: Sales_Totals_by_Amount/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sales_Totals_by_Amount/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "SaleAmount,OrderID,CompanyName,ShippedDate")] Sales_Totals_by_Amount sales_Totals_by_Amount)
        {
            if (ModelState.IsValid)
            {
                db.Sales_Totals_by_Amount.Add(sales_Totals_by_Amount);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sales_Totals_by_Amount);
        }

        // GET: Sales_Totals_by_Amount/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_Totals_by_Amount sales_Totals_by_Amount = await db.Sales_Totals_by_Amount.FindAsync(id);
            if (sales_Totals_by_Amount == null)
            {
                return HttpNotFound();
            }
            return View(sales_Totals_by_Amount);
        }

        // POST: Sales_Totals_by_Amount/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "SaleAmount,OrderID,CompanyName,ShippedDate")] Sales_Totals_by_Amount sales_Totals_by_Amount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sales_Totals_by_Amount).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sales_Totals_by_Amount);
        }

        // GET: Sales_Totals_by_Amount/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sales_Totals_by_Amount sales_Totals_by_Amount = await db.Sales_Totals_by_Amount.FindAsync(id);
            if (sales_Totals_by_Amount == null)
            {
                return HttpNotFound();
            }
            return View(sales_Totals_by_Amount);
        }

        // POST: Sales_Totals_by_Amount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sales_Totals_by_Amount sales_Totals_by_Amount = await db.Sales_Totals_by_Amount.FindAsync(id);
            db.Sales_Totals_by_Amount.Remove(sales_Totals_by_Amount);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
