﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Alphabetical_list_of_productsController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Alphabetical_list_of_products
        public async Task<ActionResult> Index()
        {
            return View(await db.Alphabetical_list_of_products.ToListAsync());
        }

        // GET: Alphabetical_list_of_products/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alphabetical_list_of_products alphabetical_list_of_products = await db.Alphabetical_list_of_products.FindAsync(id);
            if (alphabetical_list_of_products == null)
            {
                return HttpNotFound();
            }
            return View(alphabetical_list_of_products);
        }

        // GET: Alphabetical_list_of_products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Alphabetical_list_of_products/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued,CategoryName")] Alphabetical_list_of_products alphabetical_list_of_products)
        {
            if (ModelState.IsValid)
            {
                db.Alphabetical_list_of_products.Add(alphabetical_list_of_products);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(alphabetical_list_of_products);
        }

        // GET: Alphabetical_list_of_products/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alphabetical_list_of_products alphabetical_list_of_products = await db.Alphabetical_list_of_products.FindAsync(id);
            if (alphabetical_list_of_products == null)
            {
                return HttpNotFound();
            }
            return View(alphabetical_list_of_products);
        }

        // POST: Alphabetical_list_of_products/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued,CategoryName")] Alphabetical_list_of_products alphabetical_list_of_products)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alphabetical_list_of_products).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(alphabetical_list_of_products);
        }

        // GET: Alphabetical_list_of_products/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alphabetical_list_of_products alphabetical_list_of_products = await db.Alphabetical_list_of_products.FindAsync(id);
            if (alphabetical_list_of_products == null)
            {
                return HttpNotFound();
            }
            return View(alphabetical_list_of_products);
        }

        // POST: Alphabetical_list_of_products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Alphabetical_list_of_products alphabetical_list_of_products = await db.Alphabetical_list_of_products.FindAsync(id);
            db.Alphabetical_list_of_products.Remove(alphabetical_list_of_products);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
