﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NorthWind_Project.Models;

namespace NorthWind_Project.Controllers
{
    public class Summary_of_Sales_by_QuarterController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Summary_of_Sales_by_Quarter
        public async Task<ActionResult> Index()
        {
            return View(await db.Summary_of_Sales_by_Quarter.ToListAsync());
        }

        // GET: Summary_of_Sales_by_Quarter/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter = await db.Summary_of_Sales_by_Quarter.FindAsync(id);
            if (summary_of_Sales_by_Quarter == null)
            {
                return HttpNotFound();
            }
            return View(summary_of_Sales_by_Quarter);
        }

        // GET: Summary_of_Sales_by_Quarter/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Summary_of_Sales_by_Quarter/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ShippedDate,OrderID,Subtotal")] Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter)
        {
            if (ModelState.IsValid)
            {
                db.Summary_of_Sales_by_Quarter.Add(summary_of_Sales_by_Quarter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(summary_of_Sales_by_Quarter);
        }

        // GET: Summary_of_Sales_by_Quarter/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter = await db.Summary_of_Sales_by_Quarter.FindAsync(id);
            if (summary_of_Sales_by_Quarter == null)
            {
                return HttpNotFound();
            }
            return View(summary_of_Sales_by_Quarter);
        }

        // POST: Summary_of_Sales_by_Quarter/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ShippedDate,OrderID,Subtotal")] Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(summary_of_Sales_by_Quarter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(summary_of_Sales_by_Quarter);
        }

        // GET: Summary_of_Sales_by_Quarter/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter = await db.Summary_of_Sales_by_Quarter.FindAsync(id);
            if (summary_of_Sales_by_Quarter == null)
            {
                return HttpNotFound();
            }
            return View(summary_of_Sales_by_Quarter);
        }

        // POST: Summary_of_Sales_by_Quarter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Summary_of_Sales_by_Quarter summary_of_Sales_by_Quarter = await db.Summary_of_Sales_by_Quarter.FindAsync(id);
            db.Summary_of_Sales_by_Quarter.Remove(summary_of_Sales_by_Quarter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
